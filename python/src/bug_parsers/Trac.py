'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

def parse_bug(parser):
    doc = parser.find('div', id='ticket')
    if doc:
        summary = doc.find('h2', 'summary').getText(' ')
        description = doc.find('div', 'description').find('div', 'searchable').getText(' ')

        return summary, description, None, None
    return None
