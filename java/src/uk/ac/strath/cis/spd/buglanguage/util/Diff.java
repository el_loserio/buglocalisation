package uk.ac.strath.cis.spd.buglanguage.util;

public class Diff {
    private String file;
    private int oldFrom;
    private int oldTo;
    private int newFrom;
    private int newTo;

    public Diff(String file, String oldFrom, String oldLength, String newFrom, String newLength) {
        super();
        this.file = file;
        this.oldFrom = Integer.parseInt(oldFrom);
        this.oldTo = this.oldFrom + parseInt(oldLength);
        this.newFrom = Integer.parseInt(newFrom);
        this.newTo = this.newFrom + parseInt(newLength);
    }

    private int parseInt(String in) {
        if(in == null){
            return 0;
        }
        return Integer.parseInt(in);
    }

    public String getFile() {
        return file;
    }

    public int getFrom(boolean old) {
        return old ? oldFrom : newFrom;
    }

    public int getTo(boolean old) {
        return old ? oldTo : newTo;
    }
}