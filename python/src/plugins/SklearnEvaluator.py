'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from numpy import bincount, column_stack, delete, genfromtxt, load, ma, savetxt
from os import path
from Plugin import Plugin

class SklearnEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.data_file = path.join(self.temporary_output, self.config.get('data.source', 'SklearnData') + '.npy')
        self.evaluations = [x.strip() for x in self.config['evaluations'].split(',')]
        if not self.initialise:
            self.in_file = path.join(self.report_output, 'ResultsCombiner.out')
            self.out_file = path.join(self.report_output, self.plugin_id + '.out')
            self.model_file = path.join(self.report_output, self.plugin_id + '.model')

    def load_file(self, filename):
        data = genfromtxt(filename,
                          missing_values='NA',
                          usemask=True,
                         )
        return ma.masked_invalid(data).filled(0)

    def load_data(self):
        try:
            existing = load(self.data_file)
        except IOError as e:
            self.log('No data to train on')
            return None
        training = existing[...,3:-1] # Strip off bug number, event_id, method number, and result
        training_results = existing[...,-1].astype(int)
        try:
            testing = self.load_file(self.in_file)
        except IOError as e:
            self.log('No data to test')
            return None
        method_ids = testing[...,0]
        testing = testing[...,1:]
        for i, e in enumerate(self.evaluations):
            if e == '_':
                training = delete(training, i, 1)
                testing = delete(testing, i, 1)
                del self.evaluations[i]
        return training, training_results, testing, method_ids

    def evaluate_model(self, training, training_results, testing, method_ids):
        model = self.create_model()
        if self.log_debug:
            self.debug('Training %s on %s examples. Counts: %s' % (self.bug, training_results.shape[0], bincount(training_results)))
        try:
            self.fit(model, training, training_results)
        except ValueError:
            self.log(training)
            self.log(training_results)
            raise

        results = self.predict(model, testing)
        results = column_stack((method_ids, results))
        savetxt(self.out_file, results, delimiter='\t', fmt=['%d', '%.4f'])
        with open(self.model_file, 'w') as f:
            self.write_model(f, model)

    def fit(self, model, training, training_results):
        model.fit(training, training_results)

    def predict(self, model, testing):
        return model.predict(testing)

    def write_model(self, f, model):
        for k, v in self.get_model_details(model):
            f.write('\t'.join((k, str(v))) + '\n')

    def execute(self):
        data = self.load_data()
        if data:
            training, training_results, testing, method_ids = data
            self.evaluate_model(training, training_results, testing, method_ids)
