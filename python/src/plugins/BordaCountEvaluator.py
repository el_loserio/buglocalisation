'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from numpy import amin, amax, argsort, apply_along_axis, column_stack, count_nonzero, genfromtxt, ma, savetxt, sum, where, zeros
from os import path
from Plugin import Plugin, run

class BordaCountEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.modified = self.config.get('modified') == 'true'
        self.evaluations = [x.strip() for x in self.config['evaluations'].split(',')]
        if not self.initialise:
            self.in_file = path.join(self.report_output, 'ResultsCombiner.out')
            self.out_file = path.join(self.report_output, self.plugin_id + '.out')
            self.model_file = path.join(self.report_output, self.plugin_id + '.model')

    def load_file(self, filename):
        data = genfromtxt(filename,
                          missing_values='NA',
                          usemask=True,
                         )
        return ma.masked_invalid(data).filled(0)

    def load_data(self):
        try:
            testing = self.load_file(self.in_file)
        except IOError as e:
            self.log('No data to test')
            return None
        method_ids = testing[...,0]
        testing = testing[...,1:]
        for i, e in enumerate(self.evaluations):
            if e == '_':
                testing = delete(testing, i, 1)
                del self.evaluations[i]
        return testing, method_ids

    def debug_array(self, name, x):
        if len(x.shape) > 1:
            self.debug(name + ':', x.shape, 'Min:', amin(x, 0), 'Max:', amax(x, 0))
        else:
            self.debug(name + ':', x.shape, 'Values:', x)

    def evaluate_model(self, testing, method_ids):
        self.debug_array('Testing', testing)
        positions = argsort(argsort(1 - testing, axis=0), axis=0)
        self.debug_array('Positions', positions)
        if self.modified:
            nonzero = apply_along_axis(count_nonzero, 0, testing)
            self.debug_array('Nonzero', nonzero)
        else:
            nonzero = method_ids.shape[0]
            self.debug('Nonzero:', nonzero)
        scores = where(testing, nonzero - positions, 0)
        self.debug_array('Scores', scores)
        results = sum(scores, axis=1)
        self.debug_array('Results', results)
        results = column_stack([method_ids, results])
        savetxt(self.out_file, results, delimiter='\t', fmt=['%d', '%d'])

    def execute(self):
        data = self.load_data()
        if data:
            testing, method_ids = data
            self.evaluate_model(testing, method_ids)

if __name__ == '__main__': # pragma: no cover
    run(BordaCountEvaluator())
