package uk.ac.strath.cis.spd.buglanguage.plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import uk.ac.strath.cis.spd.buglanguage.test.TestBase;

public class MethodIdentifierTest extends TestBase {

    @Test
    public void testExecute() throws Exception {
        Map<String, List<String>> diffArgs = new HashMap<String, List<String>>();
        diffArgs.put("right_only", Arrays.asList("project_src"));
        checkPlugin(new MethodIdentifier(), "testExecute", diffArgs);
    }

}
