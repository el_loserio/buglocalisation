'''
Created on 17 Jan 2013

@author: seb10184
'''
import unittest
from events.BugDownloader import BugDownloader
from test.BaseTest import BaseTest


class BugDownloaderTest(BaseTest):

    def test_execute(self):
        self.check_event_processor(BugDownloader(),
                                   'test_execute',
                                   right_only=['bugs', 'test/events/BugFixFinder.out']
                                   )

    def test_execute_wrong_product(self):
        self.check_event_processor(BugDownloader(),
                                   'test_execute_wrong_product',
                                   right_only=['bugs', 'test/events/BugFixFinder.out'],
                                   left_only=['test/events/BugDownloader.descriptions/.empty', 'test/events/BugDownloader.summaries/.empty']
                                   )

    def test_sourceforge(self):
        self.check_event_processor(BugDownloader(),
                                   'test_sourceforge',
                                   right_only=['bugs', 'test/events/BugFixFinder.out']
                                   )

if __name__ == "__main__":
    unittest.main()