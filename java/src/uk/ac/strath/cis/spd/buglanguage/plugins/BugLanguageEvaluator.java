package uk.ac.strath.cis.spd.buglanguage.plugins;

/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public
 *   License
 *   along with this program.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/*
 * FrmEvaluation.java
 * Copyright Steven Davies 2012
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;
import weka.classifiers.functions.SMO;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

public class BugLanguageEvaluator extends Plugin {
    private static final String TRUE = String.valueOf(true);
    private static final String FALSE = String.valueOf(false);
    private static final List<String> RELEVANCE = Arrays.asList(TRUE, FALSE);
    private static final NumberFormat FORMAT;
    static {
        FORMAT = NumberFormat.getInstance();
        FORMAT.setMaximumFractionDigits(5);
    }

    private File bugDescriptionsDir;
    private File results;
    private List<MethodSignature> methodSignatures;
    private Random random;
    private int seed;

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.bugDescriptionsDir = new File(temporaryOutput, "BugCleaner.corpus.out");
        this.results = new File(reportOutput, "BugLanguageEvaluator.out");
        this.seed = Integer.parseInt(config.getProperty("seed"));
        this.random = new Random(this.seed);
        return config;
    }

    @Override
    public void execute() throws Exception {
        this.methodSignatures = parseMethodIdList(new File(temporaryOutput, "MethodIdentifier.out"));
        if(!new File(bugDescriptionsDir, this.bug).exists()) {
            log("No description for " + this.bug);
            return;
        }

        if(!reportOutput.exists()) {
            reportOutput.mkdir();
        }

        int thisBug = Integer.parseInt(this.bug);
        List<String> allBugIds = new ArrayList<String>();
        for(File bug: getPreviousBugs()){
            if(new File(bugDescriptionsDir, bug.getName()).exists()){
                allBugIds.add(bug.getName());
            }
        }
        allBugIds.add(bug);
        Collections.sort(allBugIds);
        Instances allBugs = createDataset(allBugIds);
        ListMultimap<String, String> bugIdsByMethodSignature = ArrayListMultimap.create();
        Map<String, Integer> positionsByBugId = new HashMap<String, Integer>();
        int position = 0;
        for(String bug: allBugIds) {
            int otherBug = Integer.parseInt(bug);
            if (otherBug < thisBug) {
                addBugLinks(bugIdsByMethodSignature, bug);
            }
            addBugInstance(allBugs, bug);
            positionsByBugId.put(bug, position);
            position++;
        }
        allBugs = filterBugs(allBugs);

        int numIrrelevant = 1 + (int)(allBugIds.size() * 0.05);

        PrintWriter out = null;
        try {
            out = new PrintWriter(results);
            for(MethodSignature s: methodSignatures) {
                String methodSignature = s.getSignature();
                int methodId = s.getId();

                double prediction = 0;
                List<String> relevantBugs = bugIdsByMethodSignature.get(methodSignature);
                if(relevantBugs != null && !relevantBugs.isEmpty()) {
                    List<String> irrelevantBugs = sampleIrrelevantBugs(allBugIds, relevantBugs, numIrrelevant);
                    if(irrelevantBugs != null && !irrelevantBugs.isEmpty()) {
                        Instances bugs = new Instances(allBugs, relevantBugs.size() + irrelevantBugs.size());
                        copyBugs(allBugs, positionsByBugId, relevantBugs, bugs, TRUE);
                        copyBugs(allBugs, positionsByBugId, irrelevantBugs, bugs, FALSE);
                        prediction = classifyBug(allBugs, positionsByBugId, bugs);
                    }
                    else {
                        prediction = 1;
                    }
                }
                out.println(tsv(methodId, FORMAT.format(prediction)));
            }
        }
        finally {
            IOUtils.closeQuietly(out);
        }
    }

    private void addBugLinks(ListMultimap<String, String> bugIdsByMethodSignature, String bug) throws IOException {
        for(String signature: getRelatedMethods(new File(current, bug))) {
            bugIdsByMethodSignature.put(signature, bug);
        }
    }

    private Instances createDataset(List<String> allBugIds) {
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        attributes.add(new Attribute("relevant", RELEVANCE));
        attributes.add(new Attribute("description", (ArrayList<String>)null));
        Instances allBugs = new Instances("bugs", attributes, allBugIds.size());
        allBugs.setClassIndex(0);
        return allBugs;
    }

    private double classifyBug(Instances allBugs, Map<String, Integer> positionsByBugId, Instances bugs) throws Exception {
        debug("Training SMO with " + bugs.size() + " bugs with " + bugs.numAttributes() + " attributes");
        SMO classifier = new SMO();
        classifier.setRandomSeed(this.seed);
        classifier.setFilterType(new SelectedTag(SMO.FILTER_NONE, SMO.TAGS_FILTER));
        classifier.setBuildLogisticModels(true);
        classifier.buildClassifier(bugs);
        Instance test = allBugs.get(positionsByBugId.get(this.bug));
        double[] predictions = classifier.distributionForInstance(test);
        debug("Classified " + bug + " as " + Arrays.toString(predictions));
        return predictions[RELEVANCE.indexOf(TRUE)];
    }

    private List<String> sampleIrrelevantBugs(List<String> allBugIds, List<String> relevantBugs, int numIrrelevant) {
        List<String> irrelevantBugs = new ArrayList<String>(allBugIds);
        irrelevantBugs.removeAll(relevantBugs);
        irrelevantBugs.remove(this.bug);
        if(irrelevantBugs.size() < numIrrelevant) {
            return null;
        }
        Collections.shuffle(irrelevantBugs, random);
        irrelevantBugs = irrelevantBugs.subList(0, numIrrelevant);
        return irrelevantBugs;
    }

    private void copyBugs(Instances allBugs, Map<String, Integer> positionsByBugId, List<String> bugIds, Instances bugs, String relevant) {
        debug("Adding " + bugIds.size() + " bugs with class " + relevant);
        for(String bug: bugIds){
            Instance instance = allBugs.get(positionsByBugId.get(bug));
            instance.setValue(0, relevant);
            bugs.add(instance);
        }
    }

    private Instances filterBugs(Instances allBugs) throws Exception {
        StringToWordVector filter = new StringToWordVector();
        filter.setInputFormat(allBugs);
        filter.setLowerCaseTokens(true);
        filter.setTokenizer(new WordTokenizer());
        filter.setUseStoplist(true);
        filter.setIDFTransform(true);
        filter.setWordsToKeep(10000);
        filter.setAttributeNamePrefix("__");
        allBugs = Filter.useFilter(allBugs, filter);
        return allBugs;
    }

    private void addBugInstance(Instances instances, String bug) throws IOException {
        Instance instance = new DenseInstance(2);
        instance.setDataset(instances);
        instance.setValue(1, FileUtils.readFileToString(new File(bugDescriptionsDir, bug)));
        instances.add(instance);
    }

    public static void main(String[] args) throws Exception {
        run(new BugLanguageEvaluator(), args);
    }
}
