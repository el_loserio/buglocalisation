'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from calendar import timegm
from dateutil.parser import parse
import re

zones = {
        'PDT':-7 * 60 * 60,
        'PST':-8 * 60 * 60,
}

def parse_bug(parser):
    doc = parser.find('div', 'pad')
    if doc:
        summary = doc.find('h2', 'title').contents[0].strip()
        a, b = summary.split(' ', 1)
        if re.match('#\\d+', a):
            summary = b
        description = doc.find('div', id='ticket_content').find('div', 'markdown_content').getText(' ')
        time = 'NA'
        for label in doc.find('div', 'editbox').find('div', 'view_holder').findAll('label'):
            if 'Created:' in label.getText():
                time = label.findNextSibling('span')
                time = time['title'].strip()
                time = parse(time, tzinfos=zones)
                time = str(timegm(time.utctimetuple()))
                break

        return summary, description, None, time
    return None
