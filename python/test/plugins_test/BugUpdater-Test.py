'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.BugUpdater import BugUpdater
from test.BaseTest import BaseTest
import unittest

class BugUpdaterTest(BaseTest):

    def test_execute(self):
        self.check_plugin(BugUpdater(), 'test_execute', False)

if __name__ == "__main__":
    unittest.main()