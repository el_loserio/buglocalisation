'''
Created on 27 Nov 2012

@author: seb10184
'''
from Plugin import Plugin, run
from os import path
import re

SPACE = '\\s*'
PREFIX = SPACE + '(?:\\[(?:java|junit)\\])?' + SPACE
INT_GROUP = SPACE + '(\\d+)' + SPACE
FLOAT_GROUP = SPACE + '(\\d+\\.\\d+)' + SPACE
failurePattern = re.compile('^' + PREFIX +
                            'Tests run:' + INT_GROUP + ',' + SPACE +
                            'Failures?:' + INT_GROUP + ',' + SPACE +
                            'Errors?:' + INT_GROUP +
                            '(?:,' + SPACE + 'Time elapsed:' + FLOAT_GROUP + 'secs?)?' +
                            SPACE + '$')
successPattern = re.compile('^' + PREFIX +
                            'OK' + SPACE + '\\(' + INT_GROUP + 'tests?\\)' +
                            SPACE + '$')
endPattern = re.compile('^' + SPACE +
                        'Total time:' + SPACE +
                        '(?:' + INT_GROUP + 'hours?)?' + SPACE +
                        '(?:' + INT_GROUP + 'minutes?)?' + SPACE +
                        INT_GROUP + 'seconds?' +
                        SPACE + '$')

def write(o, k, v):
    o.write(k + ': ' + str(v) + '\n')

def parse_group(matcher, group):
    matched = matcher.group(group)
    if matched:
        return float(matched)
    return 0

class TestResultParser(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.results = path.join(self.temporary_output, 'TestRunner.out', 'TestRunner.log')
        self.output = path.join(self.temporary_output, 'TestResultParser.out')

    def execute(self):
        if path.exists(self.results):
            parsed = self.parse_test_results()
            self.write_test_results(*parsed)
        else:
            self.log('No test results for', self.revision)

    def write_test_results(self, tests, failures, errors, test_seconds, total_seconds):
        with open(self.output, 'w') as o:
            write(o, 'Tests run', tests);
            write(o, 'Failures', failures);
            write(o, 'Errors', errors);
            write(o, 'Test seconds', test_seconds);
            write(o, 'Total seconds', total_seconds);

    def parse_test_results(self):
        with open(self.results) as i:
            tests = 0
            failures = 0
            errors = 0
            test_seconds = 0
            total_seconds = 0
            last_line = ''
    
            for line in i:
                if line == last_line:
                    self.debug('Skipping duplicate line', line)
                    continue
                last_line = line

                matcher = failurePattern.match(line);
                if matcher:
                    tests += parse_group(matcher, 1)
                    failures += parse_group(matcher, 2)
                    errors += parse_group(matcher, 3)
                    test_seconds += parse_group(matcher, 4)
                    continue
    
                matcher = successPattern.match(line)
                if matcher:
                    tests += parse_group(matcher, 1)
                    continue
    
                matcher = endPattern.match(line)
                if matcher:
                    total_seconds += 60 * 60 * parse_group(matcher, 1) + 60 * parse_group(matcher, 2) + parse_group(matcher, 3)
                    continue
    
            return tests, failures, errors, test_seconds, total_seconds

if __name__ == '__main__': #pragma: no cover
    run(TestResultParser())
