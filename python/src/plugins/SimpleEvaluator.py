'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from numpy import argsort, column_stack, delete, genfromtxt, ma, savetxt, zeros
from os import path
from Plugin import Plugin, run

class SimpleEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.data_file = path.join(self.temporary_output, 'SklearnData.npy')
        self.evaluations = [x.strip() for x in self.config['evaluations'].split(',')]
        try:
            self.order = [int(x.strip()) for x in self.config['order'].split(',')]
        except KeyError:
            self.order = range(len(self.evaluations))

        if not self.initialise:
            self.in_file = path.join(self.report_output, 'ResultsCombiner.out')
            self.out_file = path.join(self.report_output, self.plugin_id + '.out')
            self.model_file = path.join(self.report_output, self.plugin_id + '.model')

    def load_file(self, filename):
        data = genfromtxt(filename,
                          missing_values='NA',
                          usemask=True,
                         )
        return ma.masked_invalid(data).filled(0)

    def load_data(self):
        try:
            testing = self.load_file(self.in_file)
        except IOError as e:
            self.log('No data to test')
            return None
        method_ids = testing[...,0]
        testing = testing[...,1:]
        for i, e in enumerate(self.evaluations):
            if e == '_':
                testing = delete(testing, i, 1)
                del self.evaluations[i]
        return testing, method_ids

    def evaluate_model(self, testing, method_ids):
        num_methods = method_ids.shape[0]
        results = zeros([num_methods])
        score = num_methods
        positions = argsort(1 - testing, axis=0)
        for position in positions:
            for i in self.order:
                method = position[i]
                if results[method] == 0 and testing[method, i] != 0:
                    results[method] = score
                    score -= 1
        results /= num_methods
        results = column_stack([method_ids, results])
        savetxt(self.out_file, results, delimiter='\t', fmt=['%d', '%.6f'])

    def execute(self):
        data = self.load_data()
        if data:
            testing, method_ids = data
            self.evaluate_model(testing, method_ids)

if __name__ == '__main__': # pragma: no cover
    run(SimpleEvaluator())
