'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from calendar import timegm
from dateutil.parser import parse

def parse_bug(parser):
    doc = parser.find('body', id='jira')
    if doc:
        summary = doc.find(['h1', 'h2'], id=['issue_header_summary', 'summary-val'])
        if summary:
            summary = summary.getText(' ')
            description = doc.find('div', id=['issue-description', 'description-val'])
            if description:
                description = description.getText(' ')
            time = doc.find('span', id='create-date').find('time')['datetime']
            time = parse(time)
            time = str(timegm(time.utctimetuple()))

            return summary, description, None, time
    return None
