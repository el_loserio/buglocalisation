#!/bin/sh

set -e

gant download_all
gant links_all
gant bugs_all
gant parse_all
gant aggregate_all
gant relevant_all
gant stats_all
gant model_all
gant classify_all
gant emma_all
gant coverage_all
gant bug_count_all
gant evaluate_all
gant combine_all
gant frm_all
gant report
