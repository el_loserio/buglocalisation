'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.TestRunner import TestRunner
from test.BaseTest import BaseTest
import unittest

class TestRunnerTest(BaseTest):

    def test_execute(self):
        self.check_plugin(TestRunner(),
                          'test_execute',
                          right_only=['project_src'] # Input file we don't need to check
                          )

if __name__ == "__main__":
    unittest.main()