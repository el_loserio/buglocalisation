'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.LinearEvaluator import LinearEvaluator
from test.BaseTest import BaseTest
import unittest

class LinearEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(LinearEvaluator(),
                          'test_execute',
                          right_only=['1', '2', '3', 'temporary', '123/ResultsCombiner.out'] # Input file we don't need to check
                          )

    def test_no_previous_bugs(self):
        self.check_plugin(LinearEvaluator(),
                          'test_no_previous_bugs',
                          right_only=['temporary', '123/ResultsCombiner.out'], # Input file we don't need to check
                          left_only=['123/.empty']
                          )
if __name__ == "__main__":
    unittest.main()