'''
Created on 28 Nov 2012

@author: seb10184
'''
from ConfigParser import ConfigParser
from argparse import ArgumentParser
from os import path
from plugins import Plugin
from subprocess import Popen, PIPE
import sys

class EventProcessor(object):
    def load_config(self, args=None, cp=None):
        if not cp:
            cp = parse_config(args)
        self.config = dict(cp.items('events'))
        self.config.update(dict(cp.items('derived')))
        if cp.has_section(self.__class__.__name__):
            self.config.update(dict(cp.items(self.__class__.__name__)))
        self.config.update(dict(cp.items('override')))
        self.debug = self.config.get('debug') == 'true'
        self.events = self.config['events']

    def check_project_status(self):
        skip_code = self.config.get('skip.code.check') == 'true'
        skip_results = self.config.get('skip.results.check') == 'true'
        skip_project = self.config.get('skip.project.check') == 'true'
        with open(path.join(self.events, 'project.version')) as f:
            expected_project_version = f.read().strip()
            self.code_version = self.check_git_version('.', skip_status_check=skip_code)
            self.results_version = self.check_git_version(self.config['project_root'], skip_status_check=skip_results)
            self.project_version = self.check_git_version(path.join(self.events, 'project_src'), expected=expected_project_version, skip_status_check=skip_project)

    def check_git_version(self, cwd, expected=None, skip_status_check=False):
        proc = Popen(['git', 'rev-parse', 'HEAD'], cwd=cwd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()
        errors = []
        if proc.returncode > 0 or stderr:
            raise RuntimeError('Error obtaining git version of ' + cwd + '\nCode: ' + proc.returncode + '\nOutput: ' + stderr)
        if not stdout:
            raise RuntimeError('No output obtaining git version of ' + cwd)
        version = stdout.strip()

        if expected and expected != version:
            raise RuntimeError(cwd + ' version ' + version + ' not equal to expected ' + expected)

        proc = Popen(['git', 'status', '--porcelain'], cwd=cwd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode > 0 or stderr:
            raise RuntimeError('Error obtaining git status of ' + cwd + '\nCode: ' + proc.returncode + '\nOutput: ' + stderr)
        if stdout:
            if skip_status_check:
                version = '*' + version
            else:
                raise RuntimeError('Uncommited changes in ' + cwd + '\n' + stdout)
        return version

    def write_metadata(self, out_file):
        out_file.write('# EventProcessor: ' + self.__class__.__name__ + '\n')
        out_file.write('# Code version: ' + self.code_version + '\n')
        out_file.write('# Results version: ' + self.results_version + '\n')

def parse_config(args=None):
    if not args:
        args = sys.argv[1:]
    cp = ConfigParser()
    cp.optionxform = str
    cp.add_section('derived')
    cp.add_section('override')

    parser = ArgumentParser()
    parser.add_argument('-c', '--config', required=True, action='append')
    parser.add_argument('-o', '--override', action='append', default=[])
    parser.add_argument('-e', '--start-event')
    parser.add_argument('-r', '--resume', action='store_true')
    parser.add_argument('--path-java')
    parser.add_argument('--path-py')
    parser.add_argument('--path-pl')
    for k, v in vars(parser.parse_args(args)).items():
        cp.set('derived', k, v)
    config_files = cp.get('derived', 'config', raw=True)
    overrides = cp.get('derived', 'override', raw=True)
    for config_file in config_files:
        if not cp.read(config_file):
            raise IOError('Could not read ' + config_file)
    cp.remove_option('derived', 'config')
    for override in overrides:
        try:
            k, v = override.split(':')
            cp.set('override', k, v)
        except:
            raise IOError('Could not override ' + override)
    cp.remove_option('derived', 'override')

    project_root = path.join(cp.get('events', 'results_dir'), cp.get('events', 'project_name'))
    cp.set('derived', 'project_root', project_root)
    cp.set('derived', 'current', path.abspath(path.join(project_root, 'current')))
    cp.set('derived', 'events', path.abspath(path.join(project_root, 'events')))
    cp.set('derived', 'archive', path.abspath(path.join(project_root, 'archive')))
    cp.set('derived', 'config_file', path.abspath(path.join(cp.get('derived', 'current'), 'project.config')))
    cp.set('derived', 'processor_options', ' '.join(args))

    if cp.get('derived', 'resume', raw=True):
        last_config = Plugin.get_config(args=['', cp.get('derived', 'config_file')])
        cp.set('derived', 'start_event', last_config['event_id'])
        cp.remove_option('derived', 'resume')
    return cp
