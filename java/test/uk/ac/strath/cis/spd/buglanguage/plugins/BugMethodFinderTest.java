package uk.ac.strath.cis.spd.buglanguage.plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import uk.ac.strath.cis.spd.buglanguage.test.TestBase;

public class BugMethodFinderTest extends TestBase {

    @Test
    public void testExecute() throws Exception {
        Map<String, String> renames = new HashMap<String, String>();
        renames.put("project_src/dot_git", "project_src/.git");
        Map<String, List<String>> diffArgs = new HashMap<String, List<String>>();
        diffArgs.put("right_only", Arrays.asList("project_src"));
        checkPlugin(new BugMethodFinder(), "testExecute", renames, diffArgs);
    }

    @Test
    public void testExecuteMultipleFixes() throws Exception {
        Map<String, String> renames = new HashMap<String, String>();
        renames.put("project_src/dot_git", "project_src/.git");
        Map<String, List<String>> diffArgs = new HashMap<String, List<String>>();
        diffArgs.put("right_only", Arrays.asList("project_src"));
        checkPlugin(new BugMethodFinder(), "testExecuteMultipleFixes", renames, diffArgs);
    }

}
