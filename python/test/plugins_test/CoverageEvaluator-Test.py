'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.CoverageEvaluator import CoverageEvaluator
from test.BaseTest import BaseTest
import unittest

class CoverageEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(CoverageEvaluator(),
                          'test_execute',
                          right_only=['temporary/MethodIdentifier.out',
                                      'temporary/TestRunner.out', # Input files we don't need to check
                                      'temporary/CoverageEvaluator.xml'] # Intermediary file we don't care about
                          )

if __name__ == "__main__":
    unittest.main()