'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.SimpleCombineEvaluator import SimpleCombineEvaluator
from test.BaseTest import BaseTest
import unittest

class SimpleCombineEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(SimpleCombineEvaluator(),
                          'test_execute',
                          right_only=['123/ResultsCombiner.out'] # Input file we don't need to check
                          )

if __name__ == "__main__":
    unittest.main()