'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import run
from sklearn import tree
from SklearnEvaluator import SklearnEvaluator

class DecisionTreeEvaluator(SklearnEvaluator):
    def __init__(self):
        SklearnEvaluator.__init__(self)

    def load_config(self, args=None, cp=None):
        SklearnEvaluator.load_config(self, args, cp)
        self.model_params = {}
        self.model_params['compute_importances'] = True
        self.set_model_param('random_state', 'seed')
        self.set_model_param('max_depth', 'depth')
        self.set_model_param('min_samples_leaf', 'min_size')

    def set_model_param(self, k1, k2):
        try:
            self.model_params[k1] = int(self.config[k2])
        except (KeyError, ValueError):
            pass

    def create_model(self):
        return tree.DecisionTreeRegressor(**self.model_params)

    def get_model_details(self, model):
        for i, c in enumerate(model.feature_importances_):
            yield (self.evaluations[i], c)

if __name__ == '__main__': # pragma: no cover
    run(DecisionTreeEvaluator())
