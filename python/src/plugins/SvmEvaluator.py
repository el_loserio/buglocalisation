'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import run
from sklearn import svm
from SklearnEvaluator import SklearnEvaluator

class SvmEvaluator(SklearnEvaluator):
    def __init__(self):
        SklearnEvaluator.__init__(self)

    def load_config(self, args=None, cp=None):
        SklearnEvaluator.load_config(self, args, cp)
        self.model_params = {}
        #self.set_model_param('random_state', 'seed')

    def set_model_param(self, k1, k2):
        try:
            self.model_params[k1] = int(self.config[k2])
        except (KeyError, ValueError):
            pass

    def create_model(self):
        return svm.SVR(**self.model_params)

    def get_model_details(self, model):
        for i, e in enumerate(self.evaluations):
            yield (e, ','.join([str(x) for x in model.support_vectors_[...,i]]))

if __name__ == '__main__': # pragma: no cover
    run(SvmEvaluator())
