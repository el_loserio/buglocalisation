'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import run
from sklearn import linear_model
from SklearnEvaluator import SklearnEvaluator

class LinearEvaluator(SklearnEvaluator):
    def __init__(self):
        SklearnEvaluator.__init__(self)

    def create_model(self):
        return linear_model.LinearRegression()

    def get_model_details(self, model):
        for i, c in enumerate(model.coef_):
            yield (self.evaluations[i], c)
        yield ('Intercept', model.intercept_)

if __name__ == '__main__': # pragma: no cover
    run(LinearEvaluator())
