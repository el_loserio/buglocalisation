package uk.ac.strath.cis.spd.buglanguage.util;

public class MethodBody implements Comparable<MethodBody>{
    private String signature;
    private int startLine;
    private int endLine;

    public MethodBody(String signature, int startLine, int endLine) {
        this.signature = signature;
        this.startLine = startLine;
        this.endLine = endLine;
    }

    public String getSignature() {
        return signature;
    }

    public int getStartLine() {
        return startLine;
    }

    public int getEndLine() {
        return endLine;
    }

    public int compareTo(MethodBody other) {
        return this.getStartLine() - other.getStartLine();
    }
}
