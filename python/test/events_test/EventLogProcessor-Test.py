'''
Created on 17 Jan 2013

@author: seb10184
'''
import unittest
from events.EventLogProcessor import EventLogProcessor
from test.BaseTest import BaseTest


class EventLogProcessorTest(BaseTest):
    def test_execute(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_execute',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events']
                                   )

    def test_reprocess(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_reprocess',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events', 'test/current/empty']
                                   )

    def test_duplicates_copy(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_duplicates_copy',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events']
                                   )

    def test_duplicates_repeat(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_duplicates_repeat',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events']
                                   )

    def test_duplicates_no_repeat(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_duplicates_no_repeat',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events']
                                   )

    def test_repeat_no_duplicates(self):
        self.check_event_processor(EventLogProcessor(),
                                   'test_repeat_no_duplicates',
                                   replace_paths=True,
                                   left_only=['.gitattributes'],
                                   right_only=['scripts', 'test/events']
                                   )

if __name__ == "__main__":
    unittest.main()