'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

def parse_bug(parser):
    doc = parser.find('div', id='issuezilla')
    if doc:
        table = doc.find('table', 'axial')
        if table:
            headers = table.findAll('th')
            for header in headers:
                if header.getText(' ') == '* Summary:':
                    summary = header.nextSibling.nextSibling.getText(' ')
                    break
            description = doc.find('div', id='desc1').find('pre').getText(' ')

            return summary, description, None, None
    return None
