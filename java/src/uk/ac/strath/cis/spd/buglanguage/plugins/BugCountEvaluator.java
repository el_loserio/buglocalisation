package uk.ac.strath.cis.spd.buglanguage.plugins;

/*
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;

public class BugCountEvaluator extends Plugin {

    private List<MethodSignature> methods;
    private File results;

    @Override
    public void execute() throws IOException {
        if(!reportOutput.exists()) {
            reportOutput.mkdir();
        }

        this.methods = parseMethodIdList(new File(temporaryOutput, "MethodIdentifier.out"));
        Map<String, Integer> bugCount = new HashMap<String, Integer>();
        int numBugs = 0;
        for(File resultDir: getPreviousBugs()){
            List<String> signatures = getRelatedMethods(resultDir);
            if(signatures.isEmpty()) {
                debug("No method links exist at current time for " + resultDir);
                continue;
            }
            for(String signature: signatures) {
                Integer count = bugCount.get(signature);
                if(count == null){
                    count = 0;
                }
                bugCount.put(signature, count + 1);
            }
            numBugs++;
        }

        PrintWriter out = null;
        try{
            out = new PrintWriter(results);
            for(MethodSignature method: methods){
                Integer count = bugCount.get(method.getSignature());
                if(count == null){
                    count = 0;
                }
                double percentage = (double) count / (double) numBugs;
                out.println(tsv(method.getId(), percentage));
            }
        }
        finally{
            IOUtils.closeQuietly(out);
        }
    }

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.results = new File(reportOutput, "BugCountEvaluator.out");
        return config;
    }

    public static void main(String[] args) throws Exception {
        run(new BugCountEvaluator(), args);
    }
}
