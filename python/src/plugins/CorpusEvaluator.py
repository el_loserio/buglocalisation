'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from Plugin import Plugin, run
from gensim.corpora.hashdictionary import HashDictionary
from gensim.models.tfidfmodel import TfidfModel
from gensim.similarities.docsim import Similarity
from os import path, listdir, makedirs
from shutil import rmtree
import sys
import warnings

class FolderCorpus(object):
    def __init__(self, root, dictionary):
        self.root = root
        self.dictionary = dictionary

    def __iter__(self):
        self.filenames = []
        for f in listdir(self.root):
            try:
                self.filenames.append(int(f))
            except ValueError:
                if f == 'query': pass
                else: raise
        self.filenames.sort()
        for filename in self.filenames:
            file_path = path.join(self.root, str(filename))
            with open(file_path) as f:
                tokens = tokenize(f)
                yield self.dictionary.doc2bow(tokens)

def tokenize(f):
    return f.read().split()

class CorpusEvaluator(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def execute(self):
        tmp_dir = path.join(self.temporary_output, 'CorpusEvaluator.tmp')
        rmtree(tmp_dir, ignore_errors=True)
        makedirs(tmp_dir)
        bug_description = path.join(self.report_output, 'Preprocessor.query')
        if path.exists(bug_description):
            with open(bug_description) as f:
                dictionary = HashDictionary(debug=False)
                corpus = FolderCorpus(path.join(self.temporary_output, 'Preprocessor.out'), dictionary)
                model = TfidfModel(corpus)
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore', UserWarning)
                    index = Similarity(path.join(tmp_dir, 'index'), model[corpus], num_features=len(dictionary))
                tokens = tokenize(f)
                self.debug('Query', tokens)
                query = model[dictionary.doc2bow(tokens)]
                with open(path.join(self.report_output, 'CorpusEvaluator.out'), 'w') as out_file:
                    expected = 0
                    for i, percent in enumerate(index[query]):
                        method_id = corpus.filenames[i]
                        if method_id < expected:
                            raise RuntimeError('Unexpected method ID. Expected %s but was %s' % (expected, method_id))
                        if method_id > expected:
                            for j in range(expected, method_id):
                                out_file.write('\t'.join((str(j), 'NA')))
                                out_file.write('\n')
                        out_file.write('\t'.join((str(method_id), str(percent))))
                        out_file.write('\n')
                        expected = method_id + 1

if __name__ == '__main__': # pragma: no cover
    run(CorpusEvaluator())
