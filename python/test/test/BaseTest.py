'''
Created on 16 Jan 2013

@author: seb10184
'''
import difflib
from filecmp import dircmp
from mock import MagicMock, call, patch
from os import getcwd, mkdir, path, rename, walk
from plugins import Plugin
from shutil import copytree, rmtree
from subprocess import Popen, PIPE
import unittest

class BaseTest(unittest.TestCase):

    def assertDircmpEqual(self, src, dst, dc=None, diff_args=None):
        """Compares two directories recursively for equality.

        Keyword arguments:
        dc - A dircmp object. For the purposes of recursion, does not usually need to be supplied
        diff_args - A dictionary containing expected exceptions to the comparison. Values should be lists of relative path names. Acceptable keys are:
          left_only, right_only - Paths expected to be only in the src or dst respectively
          diff_files - Paths expected to be different in src and dst directories
          funny_files - Paths expected to be in both src and dst but which could not be compared
        e.g. {'left_only': ['common/1'] } indicates that src/common/1 should exist while dst/common/1 does not

        """
        subdirs = {}
        for k in ['left_only', 'right_only', 'funny_files', 'diff_files']:
            if k in diff_args:
                for f in diff_args[k][:]:
                    head, tail = path.split(f)
                    if head:
                        tip = None
                        while head:
                            base = head
                            tip = path.join(tail, tip) if tip else tail
                            head, tail = path.split(head)
                        if base not in subdirs:
                            subdirs[base] = {}
                        if k not in subdirs[base]:
                            subdirs[base][k] = []
                        subdirs[base][k].append(tip)
                        diff_args[k].remove(f)

        if not dc:
            dc = dircmp(src, dst)
        self.assertItemsEqual(diff_args.get('left_only', []), dc.left_only)
        self.assertItemsEqual(diff_args.get('right_only', []), dc.right_only)
        self.assertItemsEqual(diff_args.get('funny_files', []), dc.funny_files)
        try:
            self.assertItemsEqual(diff_args.get('diff_files', []), dc.diff_files)
        except AssertionError:
            for f in dc.diff_files:
                if f not in diff_args.get('diff_files', []):
                    fromfile = path.join(src, f)
                    tofile = path.join(dst, f)
                    with open(fromfile) as s, open(tofile) as t:
                        a = s.readlines()
                        b = t.readlines()
                        diff = difflib.unified_diff(a, b, fromfile, tofile)
                        for line in diff:
                            print line
            raise

        for s, subdir in dc.subdirs.items():
            self.assertDircmpEqual(path.join(src, s), path.join(dst, s), dc=subdir, diff_args=subdirs.get(s, {}))

    def check_event_processor(self, processor, test_name, replace_paths=False, renames=None, **diff_args):
        def f(name):
            return path.join('test_files', 'events_test', self.__class__.__name__, test_name, name)
        def mock_popen(*args, **kwargs):
            cmd = args[0]
            stdout = None
            if cmd[0] == 'git' and cmd[1] == 'status':
                stdout = ''
            elif cmd[0] == 'git' and cmd[1] == 'rev-parse':
                if kwargs['cwd'] == '.':
                    stdout = '1234567890abcdef1234567890abcdef12345678'
                elif kwargs['cwd'] == f('out/test'):
                    stdout = 'abcdef1234567890abcdef1234567890abcdef12'
            if stdout is not None:
                mock = MagicMock()
                mock.returncode = 0
                mock.communicate.return_value = (stdout, '')
                return mock
            return Popen(*args, **kwargs)

        with patch('events.EventProcessor.Popen', side_effect=mock_popen) as mock:
            processor.code_version = '1234567890abcdef1234567890abcdef12345678'
            processor.results_version = 'abcdef1234567890abcdef1234567890abcdef12'
            self._check(processor, f, True, replace_paths, renames, ['--config=' + f('config')], diff_args)

    def check_plugin(self, plugin, test_name, copy_src=True, renames=None, **diff_args):
        def f(name):
            return path.join('test_files', 'plugins_test', self.__class__.__name__, test_name, name)
        self._check(plugin, f, copy_src, False, renames, ['', f('config')], diff_args)

    def _check(self, subject, f, copy_src, replace_paths, renames, config_args, diff_args):
        rmtree(f('out'), ignore_errors=True)
        if copy_src:
            copytree(f('in'), f('out'))
        else:
            mkdir(f('out'))
        if not renames: renames = {}
        for k,v in renames.items():
            rename(path.join(f('out'), k), path.join(f('out'), v))
        if isinstance(subject, Plugin.Plugin):
            Plugin.run(subject, args=config_args)
        else:
            subject.load_config(args=config_args)
            subject.execute()
        for k,v in renames.items():
            rename(path.join(f('out'), v), path.join(f('out'), k))
        if replace_paths:
            for root, _, files in walk(f('out')):
                for filename in files:
                    with open(path.join(root, filename)) as i:
                        contents = i.readlines()
                    with open(path.join(root, filename), 'w') as o:
                        for line in contents:
                            o.write(line.replace(getcwd(), '<CWD>'))
        self.assertDircmpEqual(f('expected'), f('out'), diff_args=diff_args)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()