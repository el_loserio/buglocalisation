package uk.ac.strath.cis.spd.buglanguage.plugins;

/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public
 *   License
 *   along with this program.  If not, see
 *   <http://www.gnu.org/licenses/>.
 */

/*
 * FrmEvaluation.java
 * Copyright Steven Davies 2012
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.meta.ClassificationViaRegression;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class CombineEvaluator extends Plugin {
    private static final String TRUE = String.valueOf(true);
    private static final String FALSE = String.valueOf(false);
    private static final List<String> RELEVANCE = Arrays.asList(TRUE, FALSE);
    private static final NumberFormat FORMAT;
    static{
        FORMAT = NumberFormat.getInstance();
        FORMAT.setMaximumFractionDigits(5);
    }

    private File results;
    private File modelOutput;
    private Map<String, MethodSignature> methodSignatures;
    private Random random;
    private ArrayList<String> evaluations;

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.results = new File(reportOutput, "CombineEvaluator.out");
        this.modelOutput = new File(reportOutput, "CombineEvaluator.model");
        this.evaluations = new ArrayList<String>();
        for(String e: config.getProperty("evaluations").split(",") ) {
            this.evaluations.add(e.trim());
        }
        int seed = Integer.parseInt(config.getProperty("seed"));
        this.random = new Random(seed);
        return config;
    }

    @Override
    public void execute() throws Exception {
        this.methodSignatures = parseMethodIds(new File(temporaryOutput, "MethodIdentifier.out"));
        if(!reportOutput.exists()){
            reportOutput.mkdir();
        }

        List<String> allBugIds = new ArrayList<String>();
        for(File bug: getPreviousBugs()){
            allBugIds.add(bug.getName());
        }
        Collections.sort(allBugIds);

        File inFile = new File(reportOutput, "ResultsCombiner.out");
        if(!inFile.exists()) {
            log("No data to process");
            return;
        }
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        for(String e: evaluations){
            attributes.add(new Attribute(e));
        }
        attributes.add(new Attribute("relevant", RELEVANCE));
        Instances training = new Instances("methods", attributes, 0);
        training.setClassIndex(attributes.size() - 1);

        for(String bug: allBugIds){
            List<Integer> relevantIds = new ArrayList<Integer>();
            for(String signature: getRelatedMethods(new File(current, bug))) {
                relevantIds.add(methodSignatures.get(signature).getId());
            }
            addInstances(attributes, training, bug, relevantIds);
        }

        if(training.numInstances() == 0){
            log("No previous results to train on");
            return;
        }

        Instances testing = new Instances(training, 0);
        addInstances(attributes, testing, bug, null);

        ClassificationViaRegression classifier = new ClassificationViaRegression();
        classifier.setClassifier(new LinearRegression());
        classifier.buildClassifier(training);
        PrintWriter out = null;
        try{
            out = new PrintWriter(results);
            for(int i = 0; i < testing.numInstances(); i++){
                Instance instance = testing.get(i);

                double prediction = classifier.distributionForInstance(instance)[RELEVANCE.indexOf(TRUE)];
                out.println(tsv(i, FORMAT.format(prediction)));
            }
        }
        finally{
            IOUtils.closeQuietly(out);
        }

        try {
            out = new PrintWriter(modelOutput);
            String output = classifier.toString();
            String[] lines = output.split("\\n");
            Pattern pattern = Pattern.compile("(-?\\d+\\.\\d+)(?:\\s+\\*\\s+(\\w+)\\s+\\+)?");
            boolean parse = false;
            Map<String, Double> coefficients = new HashMap<String, Double>();
            for(String evaluation: evaluations){
                coefficients.put(evaluation, 0.0);
            }
            coefficients.put("Constant", 0.0);
            for(String line: lines) {
                line = line.trim();
                if(line.startsWith("Classifier for class with index")) {
                    parse = line.contains(String.valueOf(RELEVANCE.indexOf(TRUE)));
                }
                if(parse) {
                    Matcher matcher = pattern.matcher(line);
                    if(matcher.matches()) {
                        double coefficient = Double.parseDouble(matcher.group(1));
                        String evaluator = matcher.group(2);
                        if(evaluator == null) {
                            evaluator = "Constant";
                        }
                        coefficients.put(evaluator, coefficient);
                    }
                }
                out.println("# " + line);
            }
            out.println(tsv("Constant", coefficients.get("Constant")));
            for(String evaluation: evaluations){
                out.println(tsv(evaluation, coefficients.get(evaluation)));
            }
        }
        finally {
            IOUtils.closeQuietly(out);
        }
    }

    private void addInstances(ArrayList<Attribute> attributes, Instances dataset, String bug, List<Integer> relevantIds) throws IOException {
        File inFile = new File(new File(current, bug), "ResultsCombiner.out");
        if(inFile.exists()){
            for(String line: FileUtils.readLines(inFile)){
                String[] fields = line.split("\\t");
                int methodId = Integer.parseInt(fields[0]);
                if(relevantIds == null || relevantIds.contains(methodId) || random.nextDouble() < 0.05){
                    Instance instance = new SparseInstance(attributes.size());
                    instance.setDataset(dataset);
                    for(int i = 0; i < attributes.size() - 1; i++){
                        String field = fields[i + 1];
                        if(!field.equals("NA")){
                            instance.setValue(attributes.get(i), Double.parseDouble(field));
                        }
                    }
                    if(relevantIds != null){
                        instance.setClassValue(String.valueOf(relevantIds.contains(methodId)));
                    }
                    dataset.add(instance);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        run(new CombineEvaluator(), args);
    }
}
