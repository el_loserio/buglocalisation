'''
Created on 7 Jan 2013

@author: seb10184
'''
import unittest
from plugins.Plugin import Plugin

class PluginTest(unittest.TestCase):

    def test_parse_skips(self):
        plugin = Plugin()
        plugin.config = {'key': '1,2'}
        skips = plugin.parse_skips('key')
        self.assertEqual(['1', '2'], skips)

    def test_skip_revision(self):
        plugin = Plugin()
        plugin.log_debug = False
        self.assertEquals(True, plugin.skip_revision('a', ['a', 'b']))
        self.assertEquals(True, plugin.skip_revision('b', ['a', 'b']))
        self.assertEquals(False, plugin.skip_revision('c', ['a', 'b']))

if __name__ == "__main__":
    unittest.main()