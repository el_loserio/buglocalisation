package uk.ac.strath.cis.spd.buglanguage.util;

import java.io.IOException;
import java.util.Map;

import org.eclipse.jdt.core.dom.MethodDeclaration;

public interface MethodParser<T> {
    T parseMethod(String typeName, MethodDeclaration methodDeclaration, Map<String, String> typeVariables, String source) throws IOException;
}