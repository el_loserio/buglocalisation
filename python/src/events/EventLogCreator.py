'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from operator import itemgetter
from os import path
from events.EventProcessor import EventProcessor

class EventLogCreator(EventProcessor):
    def load_config(self, args=None, cp=None):
        EventProcessor.load_config(self, args, cp)
        self.bug_fixes = path.join(self.events, 'BugStateFinder.out')
        self.event_log = path.join(self.events, 'EventLogCreator.out')

    def execute(self):
        ordered = set()
        with open(self.bug_fixes) as in_file:
            for line in in_file:
                if line.startswith('#'): continue
                bug, fix, time, report_time, state, _ = line.strip().split('\t', 5)
                ordered.add((bug, 'FIX', fix, long(time)))
                ordered.add((bug, 'REPORT', state, long(report_time)))
        ordered = sorted(ordered, key=itemgetter(3))

        with open(self.event_log, 'w') as out_file:
            self.write_metadata(out_file)
            for i, event in enumerate(ordered):
                out_file.write('\t'.join([str(x) for x in (i,) + event]) + '\n')

if __name__ == '__main__': # pragma: no cover
    event_log_creator = EventLogCreator()
    event_log_creator.load_config()
    event_log_creator.check_project_status()
    event_log_creator.execute()