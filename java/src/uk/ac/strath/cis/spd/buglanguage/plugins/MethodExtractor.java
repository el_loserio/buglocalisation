package uk.ac.strath.cis.spd.buglanguage.plugins;

/**
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import uk.ac.strath.cis.spd.buglanguage.util.MethodParser;
import uk.ac.strath.cis.spd.buglanguage.util.MethodSignature;


public class MethodExtractor extends Plugin implements MethodParser<Void> {
    private Map<String, MethodSignature> methods;
    private List<Pattern> filePatterns;
    private File methodBodies;
    private List<String> updatedFiles;

    @Override
    public Void parseMethod(String typeName, MethodDeclaration methodDeclaration, Map<String, String> typeVariables, String source) throws IOException {
        String signature = getMethodSignature(typeName, methodDeclaration, typeVariables);
        int id = methods.get(signature).getId();
        String bodyText = "";
        Block body = methodDeclaration.getBody();
        if(body != null){
            bodyText = source.substring(body.getStartPosition(), body.getStartPosition() + body.getLength());
            bodyText = bodyText.trim();
            if(bodyText.startsWith("{")) bodyText = bodyText.substring(1);
            if(bodyText.endsWith("}")) bodyText = bodyText.substring(0, bodyText.length() - 1);
            bodyText = bodyText.trim();
        }

        BufferedWriter writer = null;
        try{
            File destination = new File(methodBodies, String.valueOf(id));
            writer = new BufferedWriter(new FileWriter(destination));
            writer.write(bodyText);
        }
        finally{
            IOUtils.closeQuietly(writer);
        }
        return null;
    }

    @Override
    public void execute() throws IOException {
        this.methods = parseMethodIds(new File(this.temporaryOutput, "MethodIdentifier.out"));
        if(!methodBodies.exists()){
            methodBodies.mkdir();
        }

        if(updatedFiles != null && !updatedFiles.isEmpty()) {
            for(String filename: updatedFiles){
                File file = new File(this.projectSrc, filename);
                checkAndParseFile(this.projectSrc, file, filePatterns, this);
            }
        }
        else{
            parseFiles(this.projectSrc, this.projectSrc, filePatterns, this);
        }
    }

    @Override
    public Properties loadConfig(String[] args) throws IOException {
        Properties config = super.loadConfig(args);
        this.updatedFiles = Plugin.getChangedFilenames(config);
        this.filePatterns = parsePatterns(config.getProperty("file_patterns"));
        this.methodBodies = new File(this.temporaryOutput, "MethodExtractor.out");
        return config;
    }

    public static void main(String[] args) throws Exception {
        run(new MethodExtractor(), args);
    }
}
