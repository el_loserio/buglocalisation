'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from collections import defaultdict
import errno
from Plugin import Plugin, run
from operator import itemgetter
from os import makedirs, path
from shutil import rmtree

def get_class(signature):
    return signature.split('(', 1)[0].rsplit('.', 1)[0]

class PositionSummary(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.evaluations = [e.strip() for e in self.config['evaluations'].split(',')]
        if not self.initialise:
            self.in_files = path.join(self.fix_output, 'PositionEvaluator.out')
            self.method_file = path.join(self.fix_output, 'PositionSummary.methods')
            self.class_file = path.join(self.fix_output, 'PositionSummary.classes')
            self.method_ids, self.max_method_id = self.parse_method_ids()

    def get_relevant_method_ids(self, results_file, class_methods):
        relevant_methods = []
        relevant_classes = []
        with open(results_file) as f:
            for line in f:
                signature, _, event_id = line.split('\t')
                if int(event_id) > int(self.event_id): continue

                relevant_methods += [self.method_ids[signature.strip()]]
                relevant_classes += class_methods[get_class(signature.strip())]
        return relevant_methods, relevant_classes

    def execute(self):
        results_file = path.join(self.report_output, 'BugMethodFinder.out')
        if not path.exists(results_file):
            self.log('No relevant methods for bug %s' % self.bug)
            return

        if not path.exists(self.in_files):
            self.log('No results for bug %s' % self.bug)
            return

        class_methods = defaultdict(list)
        for signature, method_id in self.method_ids.items():
            class_methods[get_class(signature)] += [method_id]

        relevant_method_ids, relevant_classes = self.get_relevant_method_ids(results_file, class_methods)
        positions = {}
        class_positions = {}
        for e in self.evaluations:
            positions[e] = ('NA', self.max_method_id)
            class_positions[e] = ('NA', self.max_method_id)
            try:
                with open(path.join(self.in_files, e + '.positions')) as in_file:
                    for line in in_file:
                        method_id, position = [int(x) for x in line.split()]
                        if method_id in relevant_method_ids and position < positions[e][1]:
                            positions[e] = (method_id, position)
                        if method_id in relevant_classes and position < class_positions[e][1]:
                            class_positions[e] = (method_id, position)
            except IOError as e:
                if e.errno == errno.ENOENT: pass
                else: raise

        with open(self.method_file, 'w') as out_file:
            for e in self.evaluations:
                method_id, position = positions[e]
                out_file.write('\t'.join((e, str(method_id), str(position))) + '\n')

        with open(self.class_file, 'w') as out_file:
            for e in self.evaluations:
                method_id, position = class_positions[e]
                out_file.write('\t'.join((e, str(method_id), str(position))) + '\n')


if __name__ == '__main__': # pragma: no cover
    run(PositionSummary())
