'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from calendar import timegm
from dateutil.parser import parse

def parse_bug(parser):
    doc = parser.find('form', id='changeform')
    if doc:
        summary = doc.find('span', id='short_desc_nonedit_display').getText(' ')
        product = doc.find('td', id='field_container_product').getText(' ')
        comment = doc.find('div', 'bz_first_comment')
        description = comment.find('pre').getText(' ')
        time = comment.find('span', 'bz_comment_time').getText(' ')
        time = parse(time)
        time = str(timegm(time.utctimetuple()))

        return summary, description, product, time
    return None
