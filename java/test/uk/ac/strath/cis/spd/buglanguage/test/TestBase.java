package uk.ac.strath.cis.spd.buglanguage.test;

import static junit.framework.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.AssertionFailedError;

import org.apache.commons.io.FileUtils;

import uk.ac.strath.cis.spd.buglanguage.plugins.Plugin;
import difflib.Delta;
import difflib.DiffUtils;

public abstract class TestBase {
    public void assertDirEquals(File src, File dst, Map<String, List<String>> diffArgs) throws IOException {
        Map<String, Map<String, List<String>>> subdirs = new HashMap<String, Map<String, List<String>>>();
        if(diffArgs == null) {
            diffArgs = new HashMap<String, List<String>>();
        }
        for(String k: Arrays.asList("left_only", "right_only", "funny_files", "diff_files")) {
            if(diffArgs.containsKey(k)){
                List<String> files = diffArgs.get(k);
                diffArgs.put(k, new ArrayList<String>(files));
                for(String f: files) {
                    String head = new File(f).getParent();
                    String tail = new File(f).getName();
                    if(head != null) {
                        String base = null;
                        String tip = null;
                        while(head != null) {
                            base = head;
                            if(tip != null) {
                                tip = new File(tail, tip).getPath();
                            }
                            else {
                                tip = tail;
                            }
                            tail = new File(head).getName();
                            head = new File(head).getParent();
                        }
                        if(!subdirs.containsKey(base)) {
                            subdirs.put(base, new HashMap<String, List<String>>());
                        }
                        if(!subdirs.get(base).containsKey(k)) {
                            subdirs.get(base).put(k, new ArrayList<String>());
                        }
                        subdirs.get(base).get(k).add(tip);
                        diffArgs.get(k).remove(f);
                    }
                }
            }
            else {
                diffArgs.put(k, new ArrayList<String>());
            }
        }

        Set<String> leftOnly = new HashSet<String>(listFiles(src));
        leftOnly.removeAll(listFiles(dst));
        Set<String> rightOnly = new HashSet<String>(listFiles(dst));
        rightOnly.removeAll(listFiles(src));
        assertEquals(new HashSet<String>(diffArgs.get("left_only")), leftOnly);
        assertEquals(new HashSet<String>(diffArgs.get("right_only")), rightOnly);

        Set<String> funnyFiles = new HashSet<String>();
        Set<String> diffFiles = new HashSet<String>();
        Set<String> common = new HashSet<String>(listFiles(src));
        common.retainAll(listFiles(dst));
        for(String fileName: common) {
            File srcFile = new File(src, fileName);
            File dstFile = new File(dst, fileName);
            if(srcFile.isDirectory()) {
                if(dstFile.isDirectory()) {
                    assertDirEquals(srcFile, dstFile, subdirs.get(fileName));
                }
                else {
                    funnyFiles.add(fileName);
                }
            }
            else {
                if(dstFile.isDirectory()) {
                    funnyFiles.add(fileName);
                }
                else {
                    if(!FileUtils.contentEquals(srcFile, dstFile)) {
                        diffFiles.add(fileName);
                    }
                }
            }
        }
        assertEquals(new HashSet<String>(diffArgs.get("funny_files")), funnyFiles);
        try {
            assertEquals(new HashSet<String>(diffArgs.get("diff_files")), diffFiles);
        }
        catch(AssertionFailedError e) {
            for(String file: diffFiles) {
                if(!diffArgs.get("diff_files").contains(file)) {
                    List<String> srcContents = FileUtils.readLines(new File(src, file));
                    List<String> dstContents = FileUtils.readLines(new File(dst, file));
                    System.out.println("Differences in file " + file + ": ");
                    List<Delta> deltas = DiffUtils.diff(srcContents, dstContents).getDeltas();
                    for(Delta delta: deltas){
                        System.out.println(delta);
                    }
                }
            }
            throw e;
        }
    }

    private List<String> listFiles(File file) {
        return Arrays.asList(file.list());
    }

    protected File loadTestFile(String name) {
        String directory = this.getClass().getCanonicalName().replaceAll("\\.", "/");
        return new File(new File("test_files", directory), name);
    }

    protected void checkPlugin(Plugin plugin, String testName, Map<String, List<String>> diffArgs) throws Exception {
        checkPlugin(plugin, testName, new HashMap<String, String>(), diffArgs);
    }
    protected void checkPlugin(Plugin plugin, String testName, Map<String, String> renames, Map<String, List<String>> diffArgs) throws Exception {
        File testDir = loadTestFile(testName);
        File in = new File(testDir, "in");
        File expected = new File(testDir, "expected");
        File out = new File(testDir, "out");
        File config = new File(testDir, "config");
        FileUtils.deleteDirectory(out);
        FileUtils.copyDirectory(in, out);
        for(String k: renames.keySet()) {
            new File(out, k).renameTo(new File(out, renames.get(k)));
        }
        Plugin.run(plugin, new String[] {config.getAbsolutePath()});
        for(String k: renames.keySet()) {
            new File(out, renames.get(k)).renameTo(new File(out, k));
        }
        assertDirEquals(expected, out, diffArgs);
    }
}
