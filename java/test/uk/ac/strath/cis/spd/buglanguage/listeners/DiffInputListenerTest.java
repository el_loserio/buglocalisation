package uk.ac.strath.cis.spd.buglanguage.listeners;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import uk.ac.strath.cis.spd.buglanguage.test.TestBase;
import uk.ac.strath.cis.spd.buglanguage.util.Diff;

public class DiffInputListenerTest extends TestBase{

    @Test
    public void testDiffInputListener() throws IOException {
        DiffInputListener listener = new DiffInputListener();
        File testFile = loadTestFile("testDiffInputListener");
        for(String line: FileUtils.readLines(testFile)){
            listener.processLine(line);
        }
        List<Diff> actual = listener.getChanges();
        List<Diff> expected = new ArrayList<Diff>();
        expected.add(new Diff("b/build/aant.config", "1", "5", "1", "4"));
        expected.add(new Diff("b/build/aant.config", "7", "3", "6", "4"));
        expected.add(new Diff("b/build/build.gant", "5", "11", "5", "14"));
        expected.add(new Diff("b/build/build.gant", "17", "10", "20", "10"));
        expected.add(new Diff("b/build/build.gant", "30", "9", "33", "9"));
        expected.add(new Diff("b/build/build.gant", "46", "13", "49", "7"));
        expected.add(new Diff("b/build/build.gant", "65", "7", "62", "7"));
        expected.add(new Diff("b/build/build.gant", "153", "6", "150", "14"));
        expected.add(new Diff("b/build/build.gant", "160", "6", "165", "23"));
        expected.add(new Diff("b/build/build.gant", "196", "7", "218", "7"));
        assertEquals(expected.size(), actual.size());
        for(int i=0; i<expected.size(); i++) {
            Diff e = expected.get(i);
            Diff a = actual.get(i);
            assertEquals("File not equal for diff " + i, e.getFile(), a.getFile());
            assertEquals("New from not equal for diff " + i, e.getFrom(false), a.getFrom(false));
            assertEquals("New to not equal for diff " + i, e.getTo(false), a.getTo(false));
            assertEquals("Old from not equal for diff " + i, e.getFrom(true), a.getFrom(true));
            assertEquals("Old to not equal for diff " + i, e.getTo(true), a.getTo(true));
        }
    }

}
