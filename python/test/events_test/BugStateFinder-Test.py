'''
Created on 17 Jan 2013

@author: seb10184
'''
import unittest
from events.BugStateFinder import BugStateFinder
from os import path
from test.BaseTest import BaseTest

class BugStateFinderTest(BaseTest):

    def test_execute(self):
        self.check_event_processor(BugStateFinder(),
                                   'test_execute',
                                   renames={path.join('test', 'events', 'project_src', 'dot_git'): path.join('test', 'events', 'project_src', '.git')},
                                   right_only=['test/events/BugDownloader.out', 'test/events/project_src']
                                   )

if __name__ == "__main__":
    unittest.main()