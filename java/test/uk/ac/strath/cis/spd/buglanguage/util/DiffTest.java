package uk.ac.strath.cis.spd.buglanguage.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiffTest {
    @Test
    public void testDiff() {
        Diff diff = new Diff("file", "1", "2", "4", "5");
        assertEquals("file", diff.getFile());
        assertEquals(1, diff.getFrom(true));
        assertEquals(3, diff.getTo(true));
        assertEquals(4, diff.getFrom(false));
        assertEquals(9, diff.getTo(false));
    }

    @Test
    public void testNoLength() {
        Diff diff = new Diff("file", "1", null, "4", null);
        assertEquals(1, diff.getFrom(true));
        assertEquals(1, diff.getTo(true));
        assertEquals(4, diff.getFrom(false));
        assertEquals(4, diff.getTo(false));
    }

    @Test(expected=NumberFormatException.class)
    public void testNoTo() {
        new Diff("file", "1", "2", null, "4");
    }

    @Test(expected=NumberFormatException.class)
    public void testNoFrom() {
        new Diff("file", null, "2", "3", "4");
    }
}
