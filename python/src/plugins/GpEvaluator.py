'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

import numpy as np
from Plugin import run
from pyevolve import Consts
from pyevolve.GSimpleGA import GSimpleGA
from pyevolve.GTree import GTreeGP
import signal
from sklearn.metrics import mean_absolute_error
from SklearnEvaluator import SklearnEvaluator

class GpEvaluator(SklearnEvaluator):
    def __init__(self):
        SklearnEvaluator.__init__(self)

    def load_config(self, args=None, cp=None):
        SklearnEvaluator.load_config(self, args, cp)
        self.seed = int(self.config['seed'])
        self.fitness_function = self.config['fitness']

    def fitness(self, chromosome):
        predicted = self.predictions(chromosome, self.training)
        if not np.any(predicted): return 1
        if self.fitness_function == 'mean_absolute_error':
            return mean_absolute_error(self.training_results, predicted)
        elif self.fitness_function == 'sum_positive_percentage':
            positive_percentages = predicted * self.training_results
            maximum = sum(self.training_results)
            actual = sum(positive_percentages)
            return 1 - actual / maximum
        else:
            raise RuntimeError('Invalid fitness function')

    def create_model(self):
        genome = GTreeGP()
        genome.setParams(max_depth=4)
        genome.evaluator.set(self.fitness)

        self.terminals = {}
        for i, e in enumerate(self.evaluations):
            self.terminals['data[...,' + str(i) + ']'] = e

        ga = GSimpleGA(genome, self.seed)
        gp_terminals = self.terminals.keys()
        gp_terminals += ['ephemeral:random.randint(1,10)']
        gp_function_set = {}
        gp_function_set['np.add'] = 2
        gp_function_set['np.multiply'] = 2
        gp_function_set['np.subtract'] = 2
        #gp_function_set['np.power'] = 2
        #gp_function_set['np.exp'] = 1
        #gp_function_set['np.log'] = 1
        #gp_function_set['np.reciprocal'] = 1
        #gp_function_set['np.sin'] = 1
        #gp_function_set['np.cos'] = 1
        #gp_function_set['np.tan'] = 1
        gp_function_set['np.divide'] = 2
        #gp_function_set['np.fmod'] = 2
        #gp_function_set['np.greater'] = 2
        #gp_function_set['np.greater_equal'] = 2
        #gp_function_set['np.less'] = 2
        #gp_function_set['np.less_equal'] = 2
        #gp_function_set['np.equal'] = 2
        #gp_function_set['np.not_equal'] = 2
        #gp_function_set['np.logical_and'] = 2
        #gp_function_set['np.logical_or'] = 2
        #gp_function_set['np.logical_xor'] = 2
        #gp_function_set['np.logical_not'] = 1
        #gp_function_set['np.where'] = 3
        ga.setParams(gp_terminals = gp_terminals, gp_function_set = gp_function_set)
        ga.setMinimax(Consts.minimaxType["minimize"])

        return ga

    def get_model_details(self, model):
        yield ('function', self.format_chromosome(model.bestIndividual()))

    def format_chromosome(self, chromosome):
        expression = chromosome.getPreOrderExpression()
        for k, v in self.terminals.items():
            expression = expression.replace(k, v)
        return expression

    def fit(self, model, training, training_results):
        self.training = training
        self.training_results = training_results
        model.evolve(freq_stats=10)

    def predict(self, model, testing):
        return self.predictions(model.bestIndividual(), testing)

    def timeout(self):
        raise IOError('Timeout evaluating function')

    def predictions(self, chromosome, data):
        np.seterr(all='ignore')
        signal.signal(signal.SIGALRM, self.timeout)
        try:
            signal.alarm(1)
            calculated = eval(chromosome.getCompiledCode())
            signal.alarm(0)
            calculated = np.asarray(calculated, 'float')
            calculated = np.nan_to_num(calculated)
            cmin = calculated.min()
            cmax = calculated.max()
            if cmin == cmax: raise RuntimeError('All predicted values equal')
            normalised = (calculated - cmin) / (cmax - cmin)
            normalised = np.nan_to_num(normalised)
            return normalised
        except Exception as e:
            #self.log('Error evaluating ' + self.format_chromosome(chromosome))
            #self.log(e)
            return np.zeros(data.shape[0])

if __name__ == '__main__': # pragma: no cover
    run(GpEvaluator())
