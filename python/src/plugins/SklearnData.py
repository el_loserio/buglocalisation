'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

import errno
from math import ceil
from numpy import array, concatenate, column_stack, delete, empty, genfromtxt, load, logical_and, ones, random, row_stack, ma, save, where, zeros
from os import path, remove
from Plugin import Plugin, run

class SklearnData(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.data_file = path.join(self.temporary_output, self.plugin_id + '.npy')
        random.seed(int(self.config['seed']))
        try:
            self.event_cutoff = int(self.config['event.cutoff'])
        except KeyError:
            self.event_cutoff = None
        self.negative_sample_rate = float(self.config.get('negative.sample.rate', 0.01))
        if not self.initialise:
            self.method_ids, self.max_method_id = self.parse_method_ids()

    def init(self):
        try:
            remove(self.data_file)
        except OSError as e:
            if e.errno == errno.ENOENT: pass
            else: raise

    def load_file(self, filename):
        data = genfromtxt(filename,
                          missing_values='NA',
                          usemask=True,
                         )
        return ma.masked_invalid(data).filled(0)

    def get_relevant_indices(self, results_file):
        update = False
        indices = []
        with open(results_file) as f:
            for line in f:
                signature, _, event_id = line.split('\t')
                if int(event_id) > int(self.event_id): continue
                if int(event_id) < int(self.event_id): update = True

                indices += [self.method_ids[signature.strip()]]
        return indices, update

    def load_existing(self):
        data = load(self.data_file)
        if self.event_cutoff is None: return data
        old = where(data[...,1] < int(self.event_id) - self.event_cutoff)
        self.debug('Deleting %s old rows' % old[0].shape[0])
        return delete(data, old[0], 0)

    def update(self, indices, data_file):
        data = self.load_existing()
        for index in indices:
            row = where(logical_and(data[...,0] == int(self.bug), data[...,2] == index))
            self.debug('Update for bug %s, method %s. Updating rows %s' % (self.bug, index, row))
            if row[0].shape[0] == 0:
                new_data = self.load_file(data_file)
                new_row = where(new_data[...,0] == index)
                self.debug('Existing row not found. Adding new rows %s' % new_row)
                if new_row[0].shape[0] == 0:
                    continue
                new_data = new_data[new_row[0],]
                relevant = ones([new_data.shape[0]])
                new_data = self.create_rows(new_data, relevant)
                data = row_stack((data, new_data))
            else:
                data[row,-1] = 1
        return data

    def add_new(self, indices, data_file):
        data = self.load_file(data_file)

        num_methods = data.shape[0]
        # Should the dtype be bool?
        relevant = zeros([num_methods])
        for index in indices:
            if index < num_methods:
                relevant[index] = 1
        if not any(relevant):
            self.log('No relevant methods for bug %s' % self.bug)
            return None

        data = self.create_rows(data, relevant)
        negatives = where(relevant == 0)[0]
        nsamples = int(ceil(negatives.shape[0] * self.negative_sample_rate))
        negatives = random.choice(negatives, nsamples, False)
        positives = where(relevant == 1)[0]
        selected = concatenate((negatives, positives))
        data = data[selected]

        try:
            existing = self.load_existing()
            data = row_stack((existing, data))
        except IOError as e:
            if e.errno == errno.ENOENT: pass
            else: raise
        return data

    def create_rows(self, data, relevant):
        event = empty([data.shape[0]])
        event.fill(self.event_id)

        bug = empty([data.shape[0]])
        bug.fill(self.bug)

        return column_stack((bug, event, data, relevant))

    def execute(self):
        data_file = path.join(self.report_output, 'ResultsCombiner.out')
        results_file = path.join(self.report_output, 'BugMethodFinder.out')
        if not path.exists(data_file) or not path.exists(results_file):
            self.log('No data to train on for bug %s' % self.bug)
            return

        indices, update = self.get_relevant_indices(results_file)
        if not indices:
            self.log('No relevant methods for bug %s' % self.bug)
            return

        if update:
            data = self.update(indices, data_file)
        else:
            data = self.add_new(indices, data_file)

        if data is not None:
            save(self.data_file, data)

if __name__ == '__main__': # pragma: no cover
    run(SklearnData())

