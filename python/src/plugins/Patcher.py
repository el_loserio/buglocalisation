'''
Created on 27 Nov 2012

@author: seb10184
'''
from Plugin import Plugin, run
from os import path

class Patcher(Plugin):
    def __init(self):
        Plugin.__init__()

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.skips = self.parse_skips('skips')

    def execute(self):
        self.debug('Patching for', self.revision)

        if not self.skip_revision(self.revision, self.skips):
            patch_file = self.config['patch']
            if patch_file == 'NA':
                self.log('No patch file')
                return
            self.patch(self.project_src, patch_file)

    def patch(self, root, patch_file):
        absolute = path.abspath(patch_file)
        cmd = ['git', 'apply', '--whitespace=nowarn', absolute]
        self.execute_and_capture(cmd, root)

if __name__ == '__main__': # pragma: no cover
    run(Patcher())