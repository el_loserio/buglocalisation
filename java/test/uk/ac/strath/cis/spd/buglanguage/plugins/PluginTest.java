package uk.ac.strath.cis.spd.buglanguage.plugins;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import uk.ac.strath.cis.spd.buglanguage.test.TestBase;

public class PluginTest extends TestBase {

    private static class EmptyPlugin extends Plugin {
        @Override
        public void execute() throws Exception {
        }
    }

    @Test
    public void testLoadConfig() throws IOException {
        Plugin plugin = new EmptyPlugin();
        File testFile = loadTestFile("testLoadConfig");
        plugin.loadConfig(new String[] {testFile.getPath()});
        assertEquals(true, plugin.debug);
        assertEquals("12345", plugin.bug);
        assertEquals("0123456789abcdef0123456789abcdef01234567", plugin.revision);
        assertEquals(testFile.getParent(), plugin.current.getPath());
    }

}
