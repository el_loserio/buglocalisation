'''
Created on 9 Jan 2013

@author: seb10184
'''
from plugins.CorpusEvaluator import CorpusEvaluator
from test.BaseTest import BaseTest
import unittest

class CorpusEvaluatorTest(BaseTest):

    def test_execute(self):
        self.check_plugin(CorpusEvaluator(),
                          'test_execute',
                          right_only=['temporary', # Intermediary file we don't care about
                                      '123/Preprocessor.query' # Input file we don't need to check
                                      ])

if __name__ == "__main__":
    unittest.main()