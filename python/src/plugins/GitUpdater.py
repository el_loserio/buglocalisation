'''
Created on 11 Dec 2012

@author: seb10184
'''
from Plugin import Plugin, run
from os import mkdir, path, remove
import errno

class GitUpdater(Plugin):
    def __init__(self):
        Plugin.__init__(self)

    def load_config(self, args=None, cp=None):
        Plugin.load_config(self, args, cp)
        self.diff_results = path.join(self.temporary_output, 'GitUpdater.diff')
        self.last_revision = self.config.get('last_revision')

    def execute_quiet(self, command):
        if self.log_debug:
            try:
                command.remove('--quiet')
            except ValueError:
                pass
        self.execute_and_capture(command, self.project_src)

    def execute(self):
        self.execute_quiet(['git', 'reset', '--hard', '--quiet'])
        self.execute_quiet(['git', 'clean', '-fdx', '--quiet'])
        self.execute_quiet(['git', 'checkout', '--quiet', self.revision])

        try:
            mkdir(self.temporary_output)
        except OSError as e:
            if e.errno == errno.EEXIST: pass
            else: raise

        try:
            remove(self.diff_results)
        except OSError as e:
            if e.errno == errno.ENOENT: pass
            else: raise

        if self.last_revision:
            stdout = self.execute_and_capture(['git', 'diff', '--name-only', self.last_revision + '..' + self.revision], self.project_src)
            with open(self.diff_results, 'w') as f:
                f.write(stdout)

if __name__ == '__main__': # pragma: no cover
    run(GitUpdater())