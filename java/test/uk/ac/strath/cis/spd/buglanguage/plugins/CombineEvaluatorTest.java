package uk.ac.strath.cis.spd.buglanguage.plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import uk.ac.strath.cis.spd.buglanguage.test.TestBase;

public class CombineEvaluatorTest extends TestBase {

    @Test
    public void testExecute() throws Exception {
        Map<String, List<String>> diffArgs = new HashMap<String, List<String>>();
        diffArgs.put("right_only", Arrays.asList("1", "2", "3", "temporary", "123/ResultsCombiner.out"));
        checkPlugin(new CombineEvaluator(), "testExecute", diffArgs);
    }

    @Test
    public void testNoPreviousBugs() throws Exception {
        Map<String, List<String>> diffArgs = new HashMap<String, List<String>>();
        diffArgs.put("right_only", Arrays.asList("temporary", "123/ResultsCombiner.out"));
        diffArgs.put("left_only", Arrays.asList("123/.empty"));
        checkPlugin(new CombineEvaluator(), "testNoPreviousBugs", diffArgs);
    }

}
