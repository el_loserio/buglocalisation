package uk.ac.strath.cis.spd.buglanguage.util;

public class MethodSignature implements Comparable<MethodSignature> {
    private Integer id;
    private String signature;
    private String firstSeen;
    private String firstSeenEventId;

    public MethodSignature(int id, String signature, String firstSeen, String firstSeenEventId) {
        this.id = id;
        this.signature = signature;
        this.firstSeen = firstSeen;
        this.firstSeenEventId = firstSeenEventId;
    }

    public int getId() {
        return id;
    }

    public String getSignature() {
        return signature;
    }

    public String getFirstSeen() {
        return firstSeen;
    }

    public String getFirstSeenEventId() {
        return firstSeenEventId;
    }

    @Override
    public int compareTo(MethodSignature other) {
        return id.compareTo(other.getId());
    }
}